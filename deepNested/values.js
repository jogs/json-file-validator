const string = require('../basics/string')
const number = require('../basics/number')
const others = require('../basics/others')
const array = require('../nested/array')
const object = require('../nested/object')

module.exports = new RegExp(`(?:${others.source}|(?:${string.source})|(?:${number.source})|(?:${array.source})|(?:${object.source}))`)
