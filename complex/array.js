const values = require('./values')
const comma = /\s*\,\s*/
const secondary = new RegExp(`(?:${comma.source}(?:${values.source}))`)
const open = /\[\s*/
const close = /\s*\]/
module.exports = new RegExp(`${open.source}(?:(?:${values.source}${secondary.source}*))?${close.source}`)
