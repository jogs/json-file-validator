const string = require('../basics/string')
const number = require('../basics/number')
const others = require('../basics/others')
const array = require('../basics/array')
const object = require('../basics/object')

module.exports = new RegExp(`(?:${others.source}|(?:${string.source})|(?:${number.source})|(?:${array.source})|(?:${object.source}))`)
