# JSON file validator

Validate json files

## Validators

### basic

Only verify Array, String, Number, Boolean and NULL (NO NESTED DATA)
IF NESTING OBJECT ONLY VERIFY THE DEEPEST NODE

``` JSON
{
  "string": "text",
  "number": 15,
  "boolean": true,
  "array": [1, 2, 3]
}
```

### nested

Verify a nested JSON object (MAX 1 LEVEL)

``` JSON
{
  "string": "text",
  "number": 15,
  "boolean": true,
  "array": [1, 2, 3],
  "object1": {
    "value": 0
  }
}
```

### deep

Verify a nested JSON object (MAX 2 LEVELS)

``` JSON
{
  "string": "text",
  "number": 15,
  "boolean": true,
  "array": [[8,16,32], [{},{"a":"A"}]],
  "object1": {
    "value": 0,
    "nested":{
      "usable": true
    }
  }
}
```

### full

Verify a nested JSON object (MAX 4 LEVELS)

``` JSON
{
  "string": "text",
  "number": 15,
  "boolean": true,
  "array": [[8,16,32], [{"n": {"N":0}},{"a":"A"}]],
  "object1": {
    "value": 0,
    "nested":{
      "usable": true,
      "deep": {
        "test": true,
        "full":{
          "test": true
        }
      }
    }
  }
}
```

## Examples

_**Example 1:** validate_

```javascript
const fs = require('fs')
const validator = require('json-file-validator').basic

fs.readFile('./myFile.txt', 'utf8', (err, content) => {
  if (err) console.error(err)
  else {
    console.log((validator.test(content)) ? 'Correct' : 'Wrong')
  }
})
```

_**Example 2:** Get Valid JSON_

``` javascript
const fs = require('fs')
const validator = require('json-file-validator').basic

fs.readFile('./myFile.txt', 'utf8', (err, content) => {
  if (err) console.error(err)
  else {
    console.log(content[content.length - 1])
    let result = content.match(validator)
    result.map((json) => {
      console.log(json.replace(/\s*/, ''))
    })
  }
})
```

_**Example 3:** Convert To JS object (usable JSON)_

``` javascript
const fs = require('fs')
const validator = require('json-file-validator').basic

fs.readFile('./myFile.txt', 'utf8', (err, content) => {
  if (err) console.error(err)
  else {
    let result = content.match(validator)
    result.map((json) => {
      console.log(JSON.parse(json))
    })
  }
})
```

_**Example 4:** Clean a file_

``` text
// myFile.txt

# this is a file with comments

{
  "string": "text",
  "number": 15,
  "boolean": true,
  "array": [[8,16,32], [{"n": {"N":0}},{"a":"A"}]],
  "object1": {
    "value": 0,
    "object2":{
      "limit": false,
      "object3": {
        "limit": false,
        "object4":{
          "limit": true
        }
      }
    }
  }
}

/* footer comment */
```

```javascript
const fs = require('fs')
const validator = require('json-file-validator').full

fs.readFile('./myFile.txt', 'utf8', (err, content) => {
  if (err) console.error(err)
  else {
    let result = content.match(validator)
    result.map((json) => {
      console.log(JSON.parse(json))
    })
  }
})
```

_**Example 5:** Get Multiple JSON (in same file)_

```text
// myFile.txt

/* John Data */

{
  "name": "John",
  "lastname": "Doe",
  "age": 35
}

/* Mary Data */

{
  "name": "Mary",
  "lastname": "Doe",
  "age": 22  
}
```

```javascript
const fs = require('fs')
const validator = require('json-file-validator').basic

fs.readFile('./myFile.txt', 'utf8', (err, content) => {
  if (err) console.error(err)
  else {
    // regex for header of json element
    let header = /\s*\/*\**\s*[\w ]*\**\/*\s*/
    // regex for read HEADER and JSON
    let filter = new RegExp(`(?:(?:${header.source})(${validator.source}))+`)
    let result = content.match(filter)
    if (result) {
      result.map((text, index) => {
        console.log(index)
        // print RESULT
        console.log(text.match(validator)[0].replace(/\s*/, ''))
        console.log("______________________")
      })
    }
  }
})
```
